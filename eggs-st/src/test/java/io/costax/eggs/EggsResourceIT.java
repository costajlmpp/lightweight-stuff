package io.costax.eggs;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class EggsResourceIT {

    private Client client;
    private WebTarget tut;

    @BeforeEach
    public void init() {
        this.client = ClientBuilder.newClient();

        this.tut = this.client.target("http://localhost:8080/eggs");
    }

    @Test
    public void fetchEggs() {
        try (final Response response = this.tut.request(MediaType.APPLICATION_JSON).get()) {

            assertEquals(200, response.getStatus(), 200);

            final JsonObject egg = response.readEntity(JsonObject.class);

            assertNotNull(egg);

            System.out.println(egg);
        }
    }
}
