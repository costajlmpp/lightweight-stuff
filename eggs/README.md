# Build
mvn clean package && docker build -t io.costax/eggs .

# RUN

docker rm -f eggs || true && docker run -d -p 8080:8080 -p 4848:4848 --name eggs io.costax/eggs 