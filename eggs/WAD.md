# How to use WAD

1. copy wad.jar to project folder
2. The application serve should be running
3. define maven home: export M2_HOME=/usr/local/Cellar/maven/3.6.1/libexec
3. we can run the wad.jar using the command: java -jar "Deploy folder"



  3.1. payara
    
    ```
    ./asadmin start-domain --verbose domain1
    
    java -jar wad.jar /Users/costa/Documents/junk/app-servers/payara5/glassfish/domains/domain1/autodeploy
    ```

    
  3.2. Open Liberty 
    
    ```
    ./server start
    
    java -jar wad.jar /Users/costa/Documents/junk/app-servers/wlp/usr/servers/defaultServer/dropins
    ```
    
  3.3. wildfly
  
  ```
  java -jar wad.jar /Users/costa/Documents/junk/app-servers/wildfly/standalone/deployments
  ```
  
  
## resources

#### Get swagger documentation

    http://localhost:8080/openapi
    
#### Application metrics 

```
curl -i -XPOST -H"Content-type: text/plain" -d'black' http://localhost:8080/eggs/resources/eggs

curl -i -XGET -H"Accept: application/json" http://localhost:8080/eggs/resources/eggs

curl -i -XPOST -H"Content-type: text/plain" -d'red' http://localhost:8080/eggs/resources/eggs

http://localhost:8080/metrics/
http://localhost:8080/metrics/application

curl -i -XGET -H"Accept: application/json" http://localhost:8080/metrics/application


```
  
  
    
### notes   
export M2_HOME=/usr/local/Cellar/maven/3.6.1/libexec

java -jar wad.jar /Users/costa/Documents/junk/app-servers/payara5/glassfish/domains/domain1/autodeploy



export maven.home=/usr/local/Cellar/maven/3.6.1/libexec