package io.costax.eggs.entity;

public class Egg {

    private String color;
    private boolean boiled;

    protected Egg() {}

    private Egg(final String color, final boolean boiled) {
        this.color = color;
        this.boiled = boiled;
    }

    public static Egg createEgg(final String color, final boolean boiled) {
        return new Egg(color, boiled);
    }

    public String getColor() {
        return color;
    }

    public boolean isBoiled() {
        return boiled;
    }

    @Override
    public String toString() {
        return "Egg{" +
                "color='" + color + '\'' +
                ", boiled=" + boiled +
                '}';
    }
}
