package io.costax.eggs.control;

import io.costax.eggs.entity.Egg;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.metrics.annotation.Gauge;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ApplicationScoped
public class EggStore {

    private List<Egg> eggs;

    @PostConstruct
    private void init() {
        this.eggs = new ArrayList<>();
    }

    //@Bulkhead(value = 2, waitingTaskQueue = 5)
    //@CircuitBreaker()
    @Fallback(fallbackMethod = "burnTheEgg")
    @Retry(maxRetries = 4, delayUnit = ChronoUnit.SECONDS, delay = 1)
    public void storeEgg(final Egg egg) {

        if (eggs.size() == 5) {
            throw new StoreIsFullException("Egg store is full with 5 elements");
        }

        this.eggs.add(egg);
    }


    public void burnTheEgg(final Egg egg) {
        System.out.println("--- burning the egg: " + egg);
    }

    @Gauge(unit = "count")
    public int eggNumber() {
        return this.eggs.size();
    }

    public List<Egg> all() {
        return Collections.unmodifiableList(new ArrayList<>(this.eggs));
    }
}
