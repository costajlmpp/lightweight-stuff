package io.costax.eggs.control;

import javax.ejb.ApplicationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@ApplicationException(rollback = true)
public class StoreIsFullException extends WebApplicationException {

    public StoreIsFullException(String message) {
        super(Response.status(Response.Status.BAD_REQUEST)
                .header("reason", message)
                .build());
    }
}
