package io.costax.eggs.boundary;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

@Path("/eggs")
@Produces({MediaType.APPLICATION_JSON})
public class EggsResource {

    @Inject
    @ConfigProperty(name = "color", defaultValue = "red")
    String color;

    @Inject
    EggFactory factory;

    @Resource
    ManagedExecutorService mes;

    /**
     * this method is reactive.
     * A thread from application server does not block
     * if the factory or store is store is stock
     * and it take logger to fetching the eegs
     *
     * @param response
     */
    @GET
    public void eggs(@Suspended AsyncResponse response) {
        CompletableFuture.supplyAsync(factory::all, mes)
                .thenAccept(response::resume)
                .exceptionally(t -> this.exceptionally(response::resume, t));
        //.
        //thenAccept(response::resume).
        //orTimeout(100, TimeUnit.MILLISECONDS).
        //.exceptionally((t) -> handleTimeout(response::resume, t));
    }

    private Void exceptionally(Consumer<Response> consumer, Throwable t) {

        consumer.accept(Response.status(503).
                header("cause", "timeout in the pipeline").
                header("exception", t.toString()).
                build());
        return null;
    }

    @POST
    public void create(String color) {
        this.factory.createEgg(color);
    }
}
