package io.costax.eggs.boundary;

import io.costax.eggs.control.EggStore;
import io.costax.eggs.entity.Egg;
import org.eclipse.microprofile.metrics.MetricRegistry;
import org.eclipse.microprofile.metrics.annotation.RegistryType;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class EggFactory {

    @Inject
    EggStore store;

    @Inject
    @RegistryType(type = MetricRegistry.Type.APPLICATION)
    MetricRegistry registry;

    public void createEgg(String color) {
        if ("black".equalsIgnoreCase(color)) {
            registry.counter("rejected_colors").inc();

            return;
        }

        store.storeEgg(Egg.createEgg(color, true));
    }

    public List<Egg> all() {
        return store.all();
    }
}
